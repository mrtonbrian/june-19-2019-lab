public class Masks {
    protected long[] clearMask = new long[64];
    protected long[] setMask   = new long[64];
    public Masks() {
        // Initializes Masks To All Blank
        for (int i = 0; i < 64; i++) {
            clearMask[i] = 0l;
            setMask[i] = 0l;
        }

        // Set Mask Will Have Bit Of 1 In Each Square
        // Note: Square Will Be Represented By Bits In Each Long
        for (int i = 0; i < 64; i++) {
            setMask[i] |= (1l << i);
            //System.out.println(setMask[i]);
            // The '~' Operator Gives Complement Of Squares
            // Complement: 1 Where Board Has 0, 0 Where Board Has 1
            clearMask[i] |= ~setMask[i];
        }
    }
}
