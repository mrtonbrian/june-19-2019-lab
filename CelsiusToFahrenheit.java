// Listing 1.6
/*
Initial Code

public class ShowLogicErrors {
    public static void main(String[] args) {
        System.out.println("Celsius 35 is Fahrenheit degree ");
        System.out.println((9 / 5) * 35 + 32);
    }
}
*/

// Fixed Code
public class CelsiusToFahrenheit {
    public static void main(String[] args) {
        double degreesCelsius = Input.doubleInp("Degrees Celsius: ");
 
        // Calculates And Prints Degrees Celsius Inline
        // C To F Formula: (9/5 * C) + 32
        // Note The Use Of 9./5. Such That The Expression Evaluates To 1.8 Instead Of 1
        System.out.printf("%f degrees Celsius Is %.2f degrees Fahrenheit\n", degreesCelsius,
        (9./5.) * degreesCelsius + 32.);
    }
}