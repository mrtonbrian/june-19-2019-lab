public class Bitboard {
    public long bitBoard;

    Bitboard() {
        bitBoard = 0l;
    }

    public void printBoard(int N) {
        for (int row = 0; row < N; row++) {
            for (int col = 0; col < N; col++) {
                // Uses Ternary Operator
                System.out.print(getBit(row, col, N) ? 'Q' : '-');
            }
            System.out.println("");
        }

        System.out.println();
    }

    public static int rowColumnToSquare(int row, int column, int N) {
        return (N * row) + column;
    }

    public void clearSquare(Masks masks, int row, int column, int N) {
        clearBit(rowColumnToSquare(row, column, N), masks);
    }

    public void setSquare(Masks masks, int row, int column, int N) {
        setBit(rowColumnToSquare(row, column, N), masks);
    }

    private void clearBit(int square, Masks masks) {
        bitBoard &= masks.clearMask[square];
    }

    private void setBit(int square, Masks masks) {
        bitBoard |= masks.setMask[square];
    }

    public boolean getBit(int row, int column, int N) {
        return (bitBoard & (1l << rowColumnToSquare(row, column, N))) != 0;
    }
}
