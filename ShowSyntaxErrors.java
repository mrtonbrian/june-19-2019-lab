// Listing 1.4
/*
Initial Code:


public class ShowSyntaxErrors {
    public static main(String[] args) {
        System.out.println("Welcome to Java);
    }
}
*/

// Fixed Code
public class ShowSyntaxErrors {
    public static void main(String[] args) {
        System.out.println("Welcome to Java");
    }
}
