// Listing 1.5

/*
Initial Code:

public class ShowRuntimeErrors {
    public static void main(String[] args) {
        System.out.println(1 / 0);
    }
}

*/

// Fixed Code
public class ShowRuntimeErrors {
    public static void main(String[] args) {
        try {
            System.out.println(1 / 0);
        } catch (ArithmeticException e) {
            System.err.println("Cannot Divide By Zero!");
        }
    }
}